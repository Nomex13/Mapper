﻿namespace Iodynis.Libraries.Mapping.Example
{
    static class Program
    {
        private static void Main(params string[] arguments)
        {
            TestObject testObject = new TestObject();

            Mapper<TestObject> mapper = new Mapper<TestObject>(testObject, "test.cfg");
            mapper.Initialize();

            testObject.Size = new System.Drawing.Size(400, 500);
        }
    }
}
