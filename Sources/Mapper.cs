﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using Iodynis.Libraries.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Iodynis.Libraries.Mapping
{
    /// <summary>
    /// Mapper for the specific type.
    /// </summary>
    /// <typeparam name="T">The type.</typeparam>
	public class Mapper<T> where T: class
    {
        private static readonly CultureInfo CultureInfo = CultureInfo.InvariantCulture;
        private readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings() { Culture = CultureInfo };

        private bool IsInitialized = false;
        /// <summary>
        /// Read and bind.
        /// </summary>
        public void Initialize()
        {
            if (IsInitialized)
            {
                return;
            }

            foreach (MapperConverter converter in TypeToConverter.Values)
            {
                converter.Chain(TypeToConverter.Values);
            }
            Scan();
            DealWithBackupsAndCopies();
            Read(FilePath, Object);
            //WriteNow();
            Bind();

            IsInitialized = true;
        }
        /// <summary>
        /// Read and bind specific pass.
        /// </summary>
        /// <param name="pass">The number of the pass.</param>
        public void Initialize(int pass)
        {
            if (pass < 0)
            {
                throw new ArgumentException("Pass number cannot be negative.", nameof(pass));
            }
            if (pass == 0)
            {
                Initialize();
            }
            else
            {
                Read(FilePath, Object, pass);
            }
        }
        /// <summary>
        /// Unbind and write.
        /// </summary>
        public void Deinitialize()
        {
            if (!IsInitialized)
            {
                return;
            }

            IsInitialized = false;

            Unbind();

            // Writing thread should write immediately
            WriteNow(FilePath, Object);
        }

        /// <summary>
        /// Mode determines the way Mapper writes to the associated file.
        /// NONE:   Mapper writes to the original file. If application crashes file may end up corrupted.
        /// BACKUP: A backup file is created. Mapper writes to the original file. If application crashes the backup file will be rolled back at next startup.
        /// COPY:   A copy is created and Mapper writes to it. If application crashes nothing happens to the original file.
        /// </summary>
        public enum RecoveryModes
        {
            /// <summary>
            /// Write directly to the mapped file.
            /// </summary>
            NONE = 0,
            /// <summary>
            /// Make a backup, then write to the mapped file, then delete the backup. If at startup a backup file is found it is moved in place of the original mapped one.
            /// </summary>
            BACKUP = 1,
            /// <summary>
            /// Write to a temporary file, then delete the original one is and the temporary one is moved in its place. If at startup a temporary file is found it is deleted.
            /// </summary>
            COPY = 2,
        }

        private RecoveryModes _RecoveryMode = RecoveryModes.NONE;
        /// <summary>
        /// File writing mode.
        /// </summary>
        public RecoveryModes RecoveryMode
        {
            get
            {
                return _RecoveryMode;
            }
            set
            {
                _RecoveryMode = value;
            }
        }
        private string _RecoveryModeBackupExtension = "backup";
        /// <summary>
        /// Extension to add to the file name when creating a backup copy of the original file using the backup algorithm.
        /// </summary>
        public string RecoveryModeBackupExtension
        {
            get
            {
                return _RecoveryModeBackupExtension;
            }
            set
            {
                if (String.Compare(value, _RecoveryModeCopyExtension, CultureInfo.InvariantCulture, CompareOptions.OrdinalIgnoreCase) != 0)
                {
                    throw new Exception("Copy mode extension cannot be equal to backup mode extension.");
                }
                _RecoveryModeBackupExtension = value;
            }
        }
        private string _RecoveryModeCopyExtension = "copy";
        /// <summary>
        /// Extension to add to the file name when creating a backup copy of the original file using the copy algorithm.
        /// </summary>
        public string RecoveryModeCopyExtension
        {
            get
            {
                return _RecoveryModeCopyExtension;
            }
            set
            {
                if (String.Compare(value, _RecoveryModeBackupExtension, CultureInfo.InvariantCulture, CompareOptions.OrdinalIgnoreCase) != 0)
                {
                    throw new Exception("Copy mode extension cannot be equal to backup mode extension.");
                }
                _RecoveryModeCopyExtension = value;
            }
        }

        // TODO: Monitor file changes and call the event if it is changed externally
        ///// <summary>
        ///// Event is triggered every time the file is written.
        ///// </summary>
        //public event EventHandler<string> FileChanged;

        private readonly T Object = null;
        /// <summary>
        /// Path to the default file.
        /// Custom file paths may be specified through attributes for specific properties.
        /// </summary>
        public string FilePath { get; private set; }

        private readonly bool MapOnlyWithAttributes = true;
        private const string EncryptionPrefix = "<<<";
        private const string EncryptionPostfix = ">>>";
        /// <summary>
        /// Encryptor to use to encrypt property values.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public delegate string Encryptor(string text, string password);
        /// <summary>
        /// Decryptor to use to decrypt property values.
        /// </summary>
        public delegate string Decryptor(string text, string password);
        /// <summary>
        /// Encryptor to use to encrypt property values.
        /// </summary>
        public Encryptor EncryptionEncryptor { get; set; }
        /// <summary>
        /// Decryptor to use to decrypt property values.
        /// </summary>
        public Decryptor EncryptionDecryptor { get; set; }

        private string _EncryptionPassword = null;
        /// <summary>
        /// Password to use for encrypted values.
        /// </summary>
        public string EncryptionPassword
        {
            get
            {
                return _EncryptionPassword;
            }
            set
            {
                if (_EncryptionPassword == value)
                {
                    return;
                }
                _EncryptionPassword = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        /// <summary>
        /// The default file name.
        /// </summary>
        protected string FileNameDefault
        {
            get
            {
                return typeof(T).Name.ToLower() + ".cfg";
            }
        }

        private bool _UseQuotesForStringsByDefault = false;
        /// <summary>
        /// Always use quotes for strings even if no space characters are presented within them.
        /// </summary>
        public bool UseQuotesForStringsByDefault
        {
            get
            {
                return _UseQuotesForStringsByDefault;
            }
            set
            {
                if (_UseQuotesForStringsByDefault == value)
                {
                    return;
                }
                _UseQuotesForStringsByDefault = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        private int _TextWidth = 80;
        /// <summary>
        /// Width of the text in characters inside the file.
        /// Will try to preserve the specifed width with additional space characters.
        /// </summary>
        public int TextWidth
        {
            get
            {
                return _TextWidth;
            }
            set
            {
                if (_TextWidth == value)
                {
                    return;
                }
                _TextWidth = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        /// <summary>
        /// File access rights
        /// </summary>
        [Flags]
        public enum ReadWriteModes
        {
            /// <summary>
            /// Allow to read the file.
            /// </summary>
            READ = 1,
            /// <summary>
            /// Allow to write the file.
            /// </summary>
            WRITE = 2,
            /// <summary>
            /// Allow to read and write the file.
            /// </summary>
            READ_AND_WRITE = READ | WRITE,
        }
        /// <summary>
        /// Read and write access rights.
        /// </summary>
        public ReadWriteModes ReadAndWriteMode { get; set; } = ReadWriteModes.READ_AND_WRITE;

        private string _CommentPrefix = "# ";
        /// <summary>
        /// Comment prefix for each comment line, default is "# ".
        /// </summary>
        public string CommentPrefix
        {
            get
            {
                return _CommentPrefix;
            }
            set
            {
                if (_CommentPrefix == value)
                {
                    return;
                }
                if (value == null)
                {
                    throw new Exception("Comment prefix cannot be null.");
                }
                if (value.Contains('\r') || value.Contains('\n'))
                {
                    throw new Exception("Comment prefix cannot contain new line characters, neither \\r nor \\n.");
                }
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("Comment prefix cannot consist of whitespace characters.");
                }
                string valueTrimmed = value.Trim(new char[] { ' ', '\t' });
                if (valueTrimmed.Length != 0 && IsLineFirstSymbolValid(valueTrimmed[0]))
                {
                    throw new Exception("Comment prefix cannot start with an alphanumeric character or an underscore.");
                }
                _CommentPrefix = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        private string _True = "true";
        /// <summary>
        /// Boolean TRUE value representation, default is "true".
        /// </summary>
        public string True
        {
            get
            {
                return _True;
            }
            set
            {
                if (_True == value)
                {
                    return;
                }
                _True = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        private string _False = "false";
        /// <summary>
        /// Boolean FALSE value representation, default is "false".
        /// </summary>
        public string False
        {
            get
            {
                return _False;
            }
            set
            {
                if (_False == value)
                {
                    return;
                }
                _False = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        private string _DateTimeFormat = "yyyy'/'MM'/'dd HH':'mm':'ss.FFFFFFF";
        /// <summary>
        /// DateTime value representation, default is "yyyy/MM/dd HH:mm:ss.FFFFFFF"
        /// </summary>
        public string DateTimeFormat
        {
            get
            {
                return _DateTimeFormat;
            }
            set
            {
                if (_DateTimeFormat == value)
                {
                    return;
                }
                //if (IsInitialized)
                //{
                //    throw new Exception("Cannot set DateTime format because Mapper is already initialized.");
                //}
                _DateTimeFormat = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        private string _TimeSpanFormat = "d\\ hh\\:mm\\:ss\\.FFFFFFF";
        /// <summary>
        /// TimeSpan value representation, default is "d hh:mm:ss.FFFFFFF"
        /// </summary>
        public string TimeSpanFormat
        {
            get
            {
                return _TimeSpanFormat;
            }
            set
            {
                if (_TimeSpanFormat == value)
                {
                    return;
                }
                //if (IsInitialized)
                //{
                //    throw new Exception("Cannot set TimeSpan format because Mapper is already initialized.");
                //}
                _TimeSpanFormat = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        private string _FilePrepend = "";
        /// <summary>
        /// Start the file with the specified string.
        /// </summary>
        public string FilePrepend
        {
            get
            {
                return _FilePrepend;
            }
            set
            {
                if (_FilePrepend == value)
                {
                    return;
                }
                _FilePrepend = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }
        private string _FileAppend = "";
        /// <summary>
        /// Finish the file with the specified string.
        /// </summary>
        public string FileAppend
        {
            get
            {
                return _FileAppend;
            }
            set
            {
                if (_FileAppend == value)
                {
                    return;
                }
                _FileAppend = value;
                if (IsInitialized)
                {
                    WriteAsync();
                }
            }
        }

        /// <summary>
        /// If the binded object is changed, data will be flushed to disk not later than specified time has passed.
        /// This does not include disk access time.
        /// </summary>
        public int MaximumDelayBeforeWriteInMilliseconds { get; set; } = 10000;
        /// <summary>
        /// If the binded object is changed, data will be flushed to disk after specified time has passed in case the object doesn't change again.
        /// This delay eliminates multiple unnecessary writes to disk (e.g. when a bunch of properties is changed at once or a property is changed multiple times in a short period of time).
        /// </summary>
        public int TranquilityDelayBeforeWriteInMilliseconds { get; set; } = 1000;

        private long TimeInTicksWhenFirstWriteWasRequested;
        private long TimeInTicksWhenLastWriteWasRequested;

		private readonly Dictionary<MemberInfo, MapperAttributeBundle> AttributeBundles = new Dictionary<MemberInfo, MapperAttributeBundle>();
		private readonly Dictionary<string, List<MemberInfo>> MembersByPath = new Dictionary<string, List<MemberInfo>>();
		private readonly Dictionary<string, Dictionary<string, MemberInfo>> MemberByPathAndName = new Dictionary<string, Dictionary<string, MemberInfo>>();

        private volatile bool IsReading       = false;
        private volatile bool IsSuspended     = false;
        private volatile bool IsWritingNeeded = false;
        private volatile bool IsWriting       = false;
        private volatile bool IsWritingSoon   = false;
        private Thread ThreadWrite;

        /// <summary>
        /// Event is triggered before the file is read.
        /// </summary>
        public event EventHandler OnReadBegin;
        /// <summary>
        /// Event is triggered after the file is read.
        /// </summary>
        public event EventHandler OnReadEnd;
        /// <summary>
        /// Event is triggered before the file is written.
        /// </summary>
        public event EventHandler OnWriteBegin;
        /// <summary>
        /// Event is triggered after the file is written.
        /// </summary>
        public event EventHandler OnWriteEnd;

        //private readonly Dictionary<Type, Func<string, object>> Readers = new Dictionary<Type, Func<string, object>>();
        //private readonly Dictionary<Type, Func<object, string>> Writers = new Dictionary<Type, Func<object, string>>();
        private readonly Dictionary<Type, MapperSerializer> TypeToSerializer = new Dictionary<Type, MapperSerializer>();
        private readonly Dictionary<Type, MapperConverter> TypeToConverter = new Dictionary<Type, MapperConverter>();
        private readonly Dictionary<Type, MapperCustom> TypeToCustom = new Dictionary<Type, MapperCustom>();
        private readonly List<MapperCustom> Customs = new List<MapperCustom>();

        public Mapper()
            : this(null, null, true) { }
        public Mapper(T @object)
            : this(@object, null, true) { }
        public Mapper(T @object, bool mapOnlyMarkedProperties)
            : this(@object, null, mapOnlyMarkedProperties) { }
        public Mapper(T @object, string path)
            : this(@object, path, true) { }
        public Mapper(T @object, string path, bool mapOnlyMarkedProperties)
        {
            Object = @object;
            if (@object != null)
            {
                Assembly assembly = Assembly.GetEntryAssembly();
                string directory = Path.GetDirectoryName(Path.GetFullPath(assembly == null ? "./" : assembly.Location)) ?? "./";
                if (path != null)
                {
                    if (Directory.Exists(path))
                    {
                        FilePath = Path.Combine(path, FileNameDefault);
                    }
                    else if (File.Exists(path))
                    {
                        FilePath = path;
                    }
                    else
                    {
                        FilePath = Path.Combine(directory, path);
                    }
                }
                else
                {
                    FilePath = Path.Combine(directory, FileNameDefault);
                }
            }
            else
            {
                FilePath = path;
            }
            MapOnlyWithAttributes = mapOnlyMarkedProperties;
        }
        ~Mapper()
        {
            ;
        }
        private void Bind()
        {
            List<PropertyInfo> propertyInfos = new List<PropertyInfo>(Object.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance));
            for (int i = 0; i < propertyInfos.Count; i++)
            {
                if (Attribute.GetCustomAttribute(propertyInfos[i], typeof(MapperSection)) != null)
                {
                    Bind(propertyInfos[i]);
                }
            }
        }
        private void Unbind()
        {
            List<PropertyInfo> propertyInfos = new List<PropertyInfo>(Object.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance));
            for (int i = 0; i < propertyInfos.Count; i++)
            {
                if (Attribute.GetCustomAttribute(propertyInfos[i], typeof(MapperSection)) != null)
                {
                    Unbind(propertyInfos[i]);
                }
            }
        }
        private void Bind(PropertyInfo propertyInfo)
        {
            string eventName = propertyInfo.Name + "Changed";
            EventInfo eventInfo = typeof(T).GetEvent(eventName);
            if (eventInfo == null)
            {
                throw new Exception($"Object of type {typeof(T)} does not have event {eventName}.");
            }

            Delegate propertyValueChanged;
            if (eventInfo.EventHandlerType.GenericTypeArguments.Length == 0)
            {
                // Get generic method
                MethodInfo propertyValueChangedMethodInfoGeneric = GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).First(_methodInfo =>
                {
                    return _methodInfo.IsGenericMethod && _methodInfo.Name == nameof(OnPropertyValueChanged);
                });
                // Make specific method
                MethodInfo propertyValueChangedMethodInfo = propertyValueChangedMethodInfoGeneric.MakeGenericMethod(new Type[] { });
                // Create delegate with appropriate signature
                propertyValueChanged = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, propertyValueChangedMethodInfo);
            }
            else if (eventInfo.EventHandlerType.UnderlyingSystemType == typeof(EventHandler<>))
            {
                if (!eventInfo.EventHandlerType.GenericTypeArguments[0].IsValueType)
                {
                    Type[] propertyValueChangedMethodArguments = new[] { typeof(object), typeof(object) };
                    MethodInfo propertyValueChangedMethodInfo = GetType().GetMethod(nameof(OnPropertyValueChanged), BindingFlags.NonPublic | BindingFlags.Instance, null, propertyValueChangedMethodArguments, null);
                    propertyValueChanged = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, propertyValueChangedMethodInfo);
                }
                else
                {
                    // Convert (object, object) delegate to whatever event handler wants
                    // Get the type that we want in put into generic method
                    Type valueType = eventInfo.EventHandlerType.GenericTypeArguments[0];
                    // Get generic method
                    MethodInfo propertyValueChangedMethodInfoGeneric = GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).First(_methodInfo =>
                    {
                        return _methodInfo.IsGenericMethod && _methodInfo.Name == nameof(OnPropertyValueChanged);
                    });
                    // Make specific method
                    MethodInfo propertyValueChangedMethodInfo = propertyValueChangedMethodInfoGeneric.MakeGenericMethod(new[] { valueType });
                    // Create delegate with appropriate signature
                    propertyValueChanged = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, propertyValueChangedMethodInfo);
                }
            }
            else if (eventInfo.EventHandlerType.UnderlyingSystemType == typeof(PropertyEventHandler<,>))
            {
                if (!eventInfo.EventHandlerType.GenericTypeArguments[0].IsValueType)
                {
                    Type[] propertyValueChangedMethodArguments = new[] { typeof(object), typeof(object), typeof(object) };
                    MethodInfo propertyValueChangedMethodInfo = GetType().GetMethod(nameof(OnPropertyValueChanged), BindingFlags.NonPublic | BindingFlags.Instance, null, propertyValueChangedMethodArguments, null);
                    propertyValueChanged = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, propertyValueChangedMethodInfo);
                }
                else
                {
                    throw new Exception($"Event of type {eventInfo.EventHandlerType.UnderlyingSystemType} is not supported.");
                }
            }
            else
            {
                throw new Exception($"Event of type {eventInfo.EventHandlerType.UnderlyingSystemType} is not supported.");
            }

            // Subscribe to the event
            eventInfo.AddEventHandler(Object, propertyValueChanged);
        }
        // TODO: Implement the unbinding
        private void Unbind(PropertyInfo propertyInfo)
        {
            string eventName = propertyInfo.Name + "Changed";
            EventInfo eventInfo = typeof(T).GetEvent(eventName);
            //BinderEventHandler propertyEventHandler = OnPropertyChanged;
            //EventHandler<object> propertyEventHandler = OnPropertyChanged;
            //EventHandler<object> propertyEventHandler = delegate(object param_sender, object param_o) {  };
            //eventInfo.GetRemoveMethod().Invoke(field_object, new Object[] { propertyEventHandler });
        }
        //private void OnPropertyValueChanged<T1>(object param_object, T1 param_propertyValue)
        //{
        //    OnPropertyValueChanged(param_object, (object)param_propertyValue);
        //}
        //private void OnPropertyValueChanged(object param_object, object param_propertyValue)
        //{
        //    if (field_isReading)
        //    {
        //        return;
        //    }
        //    if (field_isSuspended)
        //    {
        //        field_isWritingNeeded = true;
        //        return;
        //    }

        //    //field_writeTimeInMilliseconds = (DateTime.Now.Ticks / 10000 /* = time in ms */ ) + field_writeDelayInMilliseconds;

        //    //if (field_isWaitingToWrite)
        //    //{
        //    //    return;
        //    //}

        //    if (field_maximumDelayBeforeWriteInMilliseconds == 0)
        //    {
        //        WriteNow(field_path, field_object);
        //    }
        //    else
        //    {
        //        WriteWhenInTranquility();//field_maximumDelayBeforeWriteInMilliseconds);
        //    }
        //}
        private void OnPropertyValueChanged<T1>(object @object, T1 propertyValue)
        {
            OnPropertyValueChanged(@object, (object)propertyValue);
        }
        private void OnPropertyValueChanged(object @object, object propertyValue)
        {
            if (IsReading)
            {
                return;
            }
            if (IsSuspended)
            {
                IsWritingNeeded = true;
                return;
            }

            WriteAsync();
        }
        /// <summary>
        /// Suspend writinig any changes to the file.
        /// </summary>
        public void Suspend()
        {
            if (!IsInitialized)
            {
                throw new Exception("Mapper is not initialized.");
            }

            IsSuspended = true;
        }
        /// <summary>
        /// Resume writinig any changes to the file.
        /// If any changes were made since the suspension they are written immediately.
        /// </summary>
        public void Resume()
        {
            if (!IsInitialized)
            {
                throw new Exception("Mapper is not initialized.");
            }

            IsSuspended = false;
            if (IsWritingNeeded)
            {
                WriteNow(FilePath, Object);
                IsWritingNeeded = false;
            }
        }

        #region Read and write
        /// <summary>
        /// Convert the <typeparamref name="T1"/> variables to <typeparamref name="T2"/> ones before the serialization. Do the inverse on the deserialization.
        /// </summary>
        /// <typeparam name="T1">The type.</typeparam>
        /// <typeparam name="T2">The intermediate type.</typeparam>
        /// <param name="onSerialization"></param>
        /// <param name="onDeserialization"></param>
        public void AddConverter<T1, T2>(Func<T1, T2> onSerialization, Func<T2, T1> onDeserialization)
        {
            if (IsInitialized)
            {
                throw new Exception("Mapper is already initialized.");
            }

            TypeToConverter.Add(typeof(T1), new MapperConverter(typeof(T1), typeof(T2), _object => onSerialization((T1)_object), _object => onDeserialization((T2)_object)));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="checker"></param>
        /// <param name="serializer"></param>
        /// <param name="deserializer"></param>
        public void AddCustom(Func<Type, bool> checker, Func<object, MemberInfo, string> serializer, Action<object, MemberInfo, string> deserializer)
        {
            if (IsInitialized)
            {
                throw new Exception("Mapper is already initialized.");
            }

            Customs.Add(new MapperCustom(checker, serializer, deserializer));
        }
        /// <summary>
        /// Add custom read and write logic for the specified type.
        /// </summary>
        /// <typeparam name="T1">The type to define the custom read and write logic.</typeparam>
        /// <param name="deserializer">Function to convert the value ffrom a string read form the file.</param>
        /// <param name="serializer">Function to convert the value to a string to write to the file.</param>
        public void AddType<T1>(Func<T1, string> serializer, Func<string, T1> deserializer)
        {
            if (IsInitialized)
            {
                throw new Exception("Mapper is already initialized.");
            }

            TypeToSerializer.Add(typeof(T1), new MapperSerializer(typeof(T1), _object => serializer((T1)_object), _string => deserializer(_string)));
        }
        /// <summary>
        /// Add custom read and write logic for the specified type.
        /// </summary>
        /// <typeparam name="T1">The type to define the custom read and write logic.</typeparam>
        /// <param name="list">List of serialized-deserialized pairs.</param>
        public void AddType<T1>(List<(string, T1)> list)
        {
            if (IsInitialized)
            {
                throw new Exception("Mapper is already initialized.");
            }

            Dictionary<string, T1> reader = new Dictionary<string, T1>();
            Dictionary<T1, string> writer = new Dictionary<T1, string>();
            foreach ((string, T1) item in list)
            {
                reader.Add(item.Item1, item.Item2);
                writer.Add(item.Item2, item.Item1);
            }

            TypeToSerializer.Add(typeof(T1), new MapperSerializer(typeof(T1),
                _object =>
                {
                    writer.TryGetValue((T1)_object, out string value);
                    return value;
                },
                _string =>
                {
                    reader.TryGetValue(_string, out T1 value);
                    return value;
                }));
        }
        /// <summary>
        /// Add custom read and write logic for the specified type.
        /// </summary>
        /// <typeparam name="T1">The type to define the custom read and write logic.</typeparam>
        /// <param name="list">List of serialized-deserialized pairs.</param>
        public void AddType<T1>(List<(T1, string)> list)
        {
            if (IsInitialized)
            {
                throw new Exception("Mapper is already initialized.");
            }

            Dictionary<string, T1> reader = new Dictionary<string, T1>();
            Dictionary<T1, string> writer = new Dictionary<T1, string>();
            foreach ((T1, string) item in list)
            {
                reader.Add(item.Item2, item.Item1);
                writer.Add(item.Item1, item.Item2);
            }

            TypeToSerializer.Add(typeof(T1), new MapperSerializer(typeof(T1),
                _object =>
                {
                    writer.TryGetValue((T1)_object, out string value);
                    return value;
                },
                _string =>
                {
                    reader.TryGetValue(_string, out T1 value);
                    return value;
                }));
            //Readers.Add(typeof(T1), _string =>
            //{
            //    reader.TryGetValue(_string, out T1 value);
            //    return value;
            //});
            //Writers.Add(typeof(T1), _object =>
            //{
            //    writer.TryGetValue((T1)_object, out string value);
            //    return value;
            //});
        }
        //private void AddTypeReader<T1>(Func<string, T1> reader)
        //{
        //    Readers.Add(typeof(T1), _string => reader(_string));
        //}
        //private void AddTypeWriter<T1>(Func<T1, string> writer)
        //{
        //    Writers.Add(typeof(T1), _object => writer((T1)_object));
        //}

		private void Scan()
		{
			{
                MemberInfo[] memberInfos = Object.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance);
				foreach (MemberInfo memberInfo in memberInfos)
				{
                    if (memberInfo.MemberType != MemberTypes.Property && memberInfo.MemberType != MemberTypes.Field)
                    {
                        continue;
                    }

					MapperAttributeBundle attributeBundle = new MapperAttributeBundle(memberInfo, 0, FilePath, "General", 0, memberInfo.Name, _UseQuotesForStringsByDefault, _EncryptionPassword);

					// Skip those with no attributes if not interested in them
					if (MapOnlyWithAttributes && !attributeBundle.HasAtLeastOneAttribute)
					{
						continue;
					}

                    // List
					AttributeBundles.Add(memberInfo, attributeBundle);

                    // Index by path
					if (!MembersByPath.ContainsKey(attributeBundle.Path))
					{
						MembersByPath.Add(attributeBundle.Path, new List<MemberInfo>());
					}
					MembersByPath[attributeBundle.Path].Add(memberInfo);

                    // Index by path and name
				    if (!MemberByPathAndName.ContainsKey(attributeBundle.Path))
				    {
				        MemberByPathAndName.Add(attributeBundle.Path, new Dictionary<string, MemberInfo>());
				    }
				    if (!MemberByPathAndName[attributeBundle.Path].ContainsKey(attributeBundle.Name))
				    {
				        MemberByPathAndName[attributeBundle.Path].Add(attributeBundle.Name, memberInfo);
				    }
                    else
                    {
                        throw new Exception($"Key {attributeBundle.Name} in file {attributeBundle.Path} is mapped tp more than one property.");
                    }

                    Type type = GetType(memberInfo);
                    foreach (var custom in Customs)
                    {
                        if (custom.Check(type))
                        {
                            TypeToCustom[type] = custom;
                            break;
                        }
                    }
				}
			}

			foreach (List<MemberInfo> propertyInfos in MembersByPath.Values)
			{
				propertyInfos.Sort(delegate(MemberInfo _memberInfoOne, MemberInfo _memberInfoTwo)
				{
					string sectionOne = AttributeBundles[_memberInfoOne].Section;
					string sectionTwo = AttributeBundles[_memberInfoTwo].Section;
					int positionOne = AttributeBundles[_memberInfoOne].Position;
					int positionTwo = AttributeBundles[_memberInfoTwo].Position;

					// Compare by section
				    int comparison = String.CompareOrdinal(sectionOne, sectionTwo);
					if (comparison != 0)
					{
						return comparison;
					}
					// Compare by position
					comparison = positionTwo - positionOne;
					if (comparison != 0)
					{
						return comparison;
					}
					// Compare by name
					comparison = String.CompareOrdinal(_memberInfoOne.Name, _memberInfoTwo.Name);
					return comparison;
				});
			}
		}
        private void DealWithBackupsAndCopies()
        {
            string pathOriginal = FilePath;
            string pathBackup = FilePath + "." + _RecoveryModeBackupExtension;
            string pathCopy = FilePath + "." + _RecoveryModeCopyExtension;

            bool existsOriginal = File.Exists(pathOriginal);
            bool existsBackup = File.Exists(pathBackup);
            bool existsCopy = File.Exists(pathCopy);

            if (_RecoveryMode == RecoveryModes.NONE)
            {
                if (!File.Exists(FilePath))
                {
                    return;
                }
            }
            else if (_RecoveryMode == RecoveryModes.BACKUP)
            {
                if (existsOriginal)
                {
                    if (existsBackup) // Got both original and its backup
                    {
                        // It isn't known if the original file was written correctly, while backup should work for sure
                        File.Delete(pathOriginal);
                        File.Move(pathBackup, pathOriginal);
                    }
                    else // There is no backup
                    {
                        ; // This is the normal case, no extra action is necessary
                    }
                }
                else // There is no original
                {
                    if (existsBackup) // But got a backup
                    {
                        // Move the backup into the place of the original file
                        File.Move(pathBackup, pathOriginal);
                    }
                    else // Neither original nor backup exist
                    {
                        if (existsCopy) // But got a copy //-V3004
                        {
                            return; // Modes do not cross each other to preserve predictability
                        }
                        else // Got absolutely nothing
                        {
                            return;
                        }
                    }
                }
            }
            else if (_RecoveryMode == RecoveryModes.COPY)
            {
                if (existsOriginal)
                {
                    if (existsCopy) // Got both original and its copy
                    {
                        // It isn't known if the copy file was written correctly, while original should work for sure
                        File.Delete(pathCopy);
                    }
                    else // There is no copy
                    {
                        ; // This is the normal case, no extra action is necessary
                    }
                }
                else // There is no original
                {
                    if (existsCopy) // But got a copy
                    {
                        // Move the copy into the place of the original file
                        File.Move(pathCopy, pathOriginal);
                    }
                    else // Neither original nor copy exist
                    {
                        if (existsBackup) // But got a backup //-V3004
                        {
                            return; // Modes do not cross each other to preserve predictability
                        }
                        else // Got absolutely nothing
                        {
                            return;
                        }
                    }
                }
            }
            else
            {
                throw new NotImplementedException($"Mode {_RecoveryMode} is not supported.");
            }
        }
        //public static void Read(string param_path, T param_object)
        //{
        //    if (param_path == null)
        //    {
        //        throw new ArgumentNullException(nameof(param_path), "Path cannot be null.");
        //    }
        //    if (!File.Exists(param_path))
        //    {
        //        throw new FileNotFoundException(param_path);
        //    }
        //    string content = File.ReadAllText(param_path, Encoding.UTF8);
        //    Parse(content, param_object);
        //}
        //public static void Parse(string param_string, T param_object)
        //{
        //    if (param_string == null)
        //    {
        //        throw new ArgumentNullException(nameof(param_string), "Content cannot be null.");
        //    }
        //    if (param_string == "")
        //    {
        //        return;
        //    }
        //    throw new NotImplementedException();
        //}
        //public static void Write(string param_path, T param_object)
        //{
        //    throw new NotImplementedException();
        //}
        private bool IsLineFirstSymbolValid(char character)
        {
            return character == '_' || ('a' <= character && character <= 'z') || ('A' <= character && character <= 'Z') || ('0' <= character && character <= '9');
        }
        /// <summary>
        /// Read the file.
        /// </summary>
        /// <param name="path">The path of the file.</param>
        /// <param name="object">The object to modify based on the values read from the file.</param>
        /// <param name="pass">The pass number.</param>
        private void Read(string path, T @object, int pass = 0)
        {
            OnReadBegin?.Invoke(this, EventArgs.Empty);
            IsReading = true;

            try
            {
                foreach (string memberPath in MembersByPath.Keys)
                {
                    Dictionary<string, MemberInfo> mamberInfoByName = MemberByPathAndName[memberPath];

                    // Nothing to read if file does not exist
                    if (!File.Exists(path))
                    {
                        continue;
                    }

                    string text = "";
                    using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 4096, FileOptions.SequentialScan))
                    using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        text = streamReader.ReadToEnd();
                    }
                    string[] lines = text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int lineIndex = 0; lineIndex < lines.Length; lineIndex++)
                    {
                        try
                        {
                            string line = lines[lineIndex].Trim(new char[] { ' ', '\t', '\r', '\n' });

                            // Empty or doesn't start with an alphanumeric character or underscore
                            if (line.Length == 0 || !IsLineFirstSymbolValid(line[0]))
                            {
                                continue;
                            }

                            string[] parts = line.Split(new char[] { ' ', '\t' }, 2).Select(lambda_part => lambda_part.Trim(new char[] { ' ', '\t', '\r', '\n' })).ToArray();

                            string keyString = parts.Length > 0 ? parts[0] : "";
                            string valueString = parts.Length > 1 ? parts[1].Replace("\u0007", "\r\n") : "";

                            if (!mamberInfoByName.ContainsKey(keyString))
                            {
                                continue;
                            }

                            MemberInfo memberInfo = mamberInfoByName[keyString];
                            MapperAttributeBundle attributeBundle = AttributeBundles[memberInfo];
                            if (attributeBundle.Pass != pass)
                            {
                                continue;
                            }

                            Type type = GetType(memberInfo);
                            bool valueCanBeNull = !type.IsValueType || (Nullable.GetUnderlyingType(type) != null);
                            if (String.IsNullOrWhiteSpace(valueString) && valueCanBeNull)
                            {
                                continue;
                            }

                            //// If null
                            //if (String.IsNullOrWhiteSpace(value))
                            //{
                            //    // If custom reader is provided
                            //    if (readers.ContainsKey(propertyInfo.PropertyType))
                            //    {
                            //        propertyInfo.SetValue(@object, readers[propertyInfo.PropertyType].Invoke(null));
                            //    }
                            //    // If value can be null
                            //    else if (valueCanBeNull)
                            //    {
                            //        propertyInfo.SetValue(@object, null);
                            //    }
                            //    continue;
                            //}

                            // If encrypted
                            if (attributeBundle.Password != null)
                            {
                                if (EncryptionDecryptor == null)
                                {
                                    throw new Exception("Decryptor is not set.");
                                }
                                try
                                {
                                    valueString = EncryptionDecryptor(valueString, attributeBundle.Password);
                                    if (valueString.Length < EncryptionPrefix.Length + EncryptionPostfix.Length ||
                                        !valueString.StartsWith(EncryptionPrefix) || !valueString.EndsWith(EncryptionPostfix))
                                    {
                                        valueString = "";
                                    }
                                    else
                                    {
                                        valueString = valueString.Substring(EncryptionPrefix.Length, valueString.Length - EncryptionPrefix.Length - EncryptionPostfix.Length);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    //value = "";
                                    throw new Exception($"Failed to decrypt.", exception);
                                }
                            }

                            if (TypeToCustom.TryGetValue(type, out var custom))
                            {
                                custom.Deserialize(@object, memberInfo, valueString);
                            }
                            else
                            {
                                object value;
                                if (TypeToConverter.TryGetValue(type, out var converter))
                                {
                                    value = Deserialize(converter.TypeFinal, valueString, attributeBundle.UseQuotes, attributeBundle.Password);
                                    value = converter.Deserialize(value);
                                }
                                else
                                {
                                    value = Deserialize(type, valueString, attributeBundle.UseQuotes, attributeBundle.Password);
                                }

                                SetValue(@object, memberInfo, value);
                            }
                        }
                        catch (Exception exception)
                        {
                            throw new Exception($"Failed to parse line {lineIndex}: {lines[lineIndex]}.", exception);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to read mapped file(s).", exception);
            }
            finally
            {
                IsReading = false;
                OnReadEnd?.Invoke(this, EventArgs.Empty);
            }
        }
        /// <summary>
        /// Move the file.
        /// </summary>
        /// <param name="path"></param>
        public void Move(string path)
        {
            if (!IsInitialized)
            {
                throw new Exception("Mapper is not initialized, thus cannot move the file.");
            }

            FilePath = path;
        }
        /// <summary>
        /// Write to the file.
        /// </summary>
        public void Write()
        {
            if (!IsInitialized)
            {
                throw new Exception("Mapper is not initialized.");
            }

            WriteNow(FilePath, Object);
        }
        /// <summary>
        /// Write the specified object values to the specified file.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <param name="object">The object to get the values from.</param>
        public void Write(string path, T @object)
        {
            if (!IsInitialized)
            {
                throw new Exception("Mapper is not initialized.");
            }

            WriteNow(path, @object);
        }
        /// <summary>
        /// Write to the file asynchronously.
        /// </summary>
        public void WriteAsync()
        {
            if (!IsInitialized)
            {
                throw new Exception("Mapper is not initialized.");
            }

            WriteWhenInTranquility();
        }
        // TODO: Rewrite this to be thread-safe
        private void WriteWhenInTranquility()
        {
            TimeInTicksWhenLastWriteWasRequested = DateTime.UtcNow.Ticks;

            // If already awaiting for writing
            if (IsWritingSoon)
            {
                return;
            }

            TimeInTicksWhenFirstWriteWasRequested = TimeInTicksWhenLastWriteWasRequested;

            IsWritingSoon = true;
            ThreadWrite = new Thread(delegate()
            {
                // Sleep the minimum required time
                if (TranquilityDelayBeforeWriteInMilliseconds > 0)
                {
                    Thread.Sleep(TranquilityDelayBeforeWriteInMilliseconds);
                }

                int delay = TranquilityDelayBeforeWriteInMilliseconds / 10 + 10;
                while (IsWriting || // Sleep while another process is writing
                    ((DateTime.UtcNow.Ticks - TimeInTicksWhenLastWriteWasRequested  < TranquilityDelayBeforeWriteInMilliseconds * 10000) && // Sleep while the time passed from the last  undone write request is lower that the threshold
                    (DateTime.UtcNow.Ticks  - TimeInTicksWhenFirstWriteWasRequested < MaximumDelayBeforeWriteInMilliseconds     * 10000)))  // Sleep while the time passed from the first undone write request is lower than the threshold
                {
                    Thread.Sleep(delay);
                }
                IsWritingSoon = false;

                IsWriting = true;
                WriteNow(FilePath, Object);
                IsWriting = false;
            });
            ThreadWrite.Name = "MapperWrite";;
            ThreadWrite.Start();
        }
        public string Serialize<T>(object value, bool useQuotes = false, string password = null)
        {
            return Serialize(typeof(T), value, useQuotes, password);
        }
        public string Serialize(Type type, object value, bool useQuotes = false, string password = null)
        {
            TypeToSerializer.TryGetValue(type, out var serializer);

            // Writer is custom-defined
            if (serializer != null)
            {
                return serializer.Serialize(value);
            }
            else if (type == typeof(string))
            {
                return FormatString((string)value, useQuotes);
            }
            else if (type == typeof(DateTime))
            {
                return FormatDateTime((DateTime)value);
            }
            else if (type == typeof(TimeSpan))
            {
                return FormatTimeSpan((TimeSpan)value);
            }
            else if (type == typeof(bool))
            {
                return (bool)value ? True : False;
            }
            else if (type.IsEnum)
            {
                return value.ToString();
            }
            else if (type == typeof(Color))
            {
                Color color = (Color)value;
                return color.A == 255 ? String.Format("#{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B) : String.Format("#{0:X2}{1:X2}{2:X2}{3:X2}", color.R, color.G, color.B, color.A);
            }
            else if (type == typeof(bool[]))
            {
                return ArrayToString(((bool[])value).Select(lambda_value => lambda_value ? True : False), false);
            }
            else if (type == typeof(byte[]))
            {
                return ArrayToString(((byte[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(char[]))
            {
                return ArrayToString(((char[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(short[]))
            {
                return ArrayToString(((short[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(ushort[]))
            {
                return ArrayToString(((ushort[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(int[]))
            {
                return ArrayToString(((int[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(uint[]))
            {
                return ArrayToString(((uint[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(long[]))
            {
                return ArrayToString(((long[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(ulong[]))
            {
                return ArrayToString(((ulong[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(float[]))
            {
                return ArrayToString(((float[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(double[]))
            {
                return ArrayToString(((double[])value).Select(lambda_value => lambda_value.ToString(CultureInfo)), false);
            }
            else if (type == typeof(string[]))
            {
                return ArrayToString(((string[])value), true);
            }
            else
            {
                // TODO: Specify serialize return value (string)
                // TODO: Specify deserialize return value (void)
                MethodInfo serialize = type.GetMethod("Serialize", BindingFlags.Public | BindingFlags.Static, null, new Type[] { type }, null);
                MethodInfo deserialize = type.GetMethod("Deserialize", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                if (serialize != null && deserialize != null)
                {
                    return (string)serialize.Invoke(null, new object[] { value });
                }
                else
                {
                    return JsonConvert.SerializeObject(value, JsonSerializerSettings).Replace("\r\n", "\u0007");
                }
            }
            if (password != null)
            {
                if (EncryptionEncryptor == null)
                {
                    throw new Exception("Encryptor is not set.");
                }
                try
                {
                    return EncryptionEncryptor(EncryptionPrefix + value.ToString() + EncryptionPostfix, password);
                }
                catch (Exception exception)
                {
                    throw new Exception($"Failed to encrypt.", exception);
                }
            }
        }
        public T Deserialize<T>(string value, bool useQuotes = false, string password = null)
        {
            return (T)Deserialize(typeof(T), value, useQuotes, password);
        }
        public object Deserialize(Type type, string value, bool useQuotes = false, string password = null)
        {
            // Reader is custom-defined
            if (TypeToSerializer.TryGetValue(type, out var serializer))
            {
                return serializer.Deserialize(value);
            }
            // If null
            else if (String.IsNullOrWhiteSpace(value) && !type.IsValueType)
            {
                return null;
            }
            else if (type.IsEnum)
            {
                return Enum.Parse(type, value);
            }
            else if (type == typeof(bool))
            {
                return String.Equals(value, True, StringComparison.OrdinalIgnoreCase);
                // TODO: Log error if neither True nor False
            }
            else if (type == typeof(byte))
            {
                return Convert.ToByte(value, CultureInfo);
            }
            else if (type == typeof(char))
            {
                return Convert.ToChar(value, CultureInfo);
            }
            else if (type == typeof(short))
            {
                return Convert.ToInt16(value, CultureInfo);
            }
            else if (type == typeof(ushort))
            {
                return Convert.ToUInt16(value, CultureInfo);
            }
            else if (type == typeof(int))
            {
                return Convert.ToInt32(value, CultureInfo);
            }
            else if (type == typeof(uint))
            {
                return Convert.ToUInt32(value, CultureInfo);
            }
            else if (type == typeof(long))
            {
                return Convert.ToInt64(value, CultureInfo);
            }
            else if (type == typeof(ulong))
            {
                return Convert.ToUInt64(value, CultureInfo);
            }
            else if (type == typeof(float))
            {
                return Convert.ToSingle(value, CultureInfo);
            }
            else if (type == typeof(double))
            {
                return Convert.ToDouble(value, CultureInfo);
            }
            else if (type == typeof(string))
            {
                return ParseString(value, useQuotes);
            }
            else if (type == typeof(DateTime))
            {
                return ParseDateTime(value, CultureInfo);
            }
            else if (type == typeof(TimeSpan))
            {
                return ParseTimeSpan(value, CultureInfo);
            }
            else if (type == typeof(string[]))
            {
                return StringToArray(value);
            }
            else if (type == typeof(bool[]))
            {
                return StringToArray(value, false).Select(lambda_value => String.Equals(lambda_value, True, StringComparison.OrdinalIgnoreCase)).ToArray<bool>();
            }
            else if (type == typeof(byte[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToByte(lambda_value, CultureInfo)).ToArray<byte>();
            }
            else if (type == typeof(char[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToChar(lambda_value, CultureInfo)).ToArray<char>();
            }
            else if (type == typeof(short[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToInt16(lambda_value, CultureInfo)).ToArray<short>();
            }
            else if (type == typeof(ushort[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToUInt16(lambda_value, CultureInfo)).ToArray<ushort>();
            }
            else if (type == typeof(int[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToInt32(lambda_value, CultureInfo)).ToArray<int>();
            }
            else if (type == typeof(uint[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToUInt32(lambda_value, CultureInfo)).ToArray<uint>();
            }
            else if (type == typeof(long[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToInt64(lambda_value, CultureInfo)).ToArray<long>();
            }
            else if (type == typeof(ulong[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToUInt64(lambda_value, CultureInfo)).ToArray<ulong>();
            }
            else if (type == typeof(float[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToSingle(lambda_value, CultureInfo)).ToArray<float>();
            }
            else if (type == typeof(double[]))
            {
                return StringToArray(value, false).Select(lambda_value => Convert.ToDouble(lambda_value, CultureInfo)).ToArray<double>();
            }
            else if (type == typeof(Color))
            {
                if ((value.Length != 7 && value.Length != 9) || value[0] != '#')
                {
                    throw new Exception($"Color {value} is invalid.");
                }
                int r = Int32.Parse(value.Substring(1, 2), NumberStyles.HexNumber);
                int g = Int32.Parse(value.Substring(3, 2), NumberStyles.HexNumber);
                int b = Int32.Parse(value.Substring(5, 2), NumberStyles.HexNumber);
                int a = value.Length == 9 ? Int32.Parse(value.Substring(7, 2), NumberStyles.HexNumber) : 255;
                return Color.FromArgb(r, g, b, a);
            }
            else
            {
                // TODO: Specify serialize return value (string)
                // TODO: Specify deserialize return value (void)
                MethodInfo serialize = type.GetMethod("Serialize", BindingFlags.Public | BindingFlags.Static, null, new Type[] { type }, null);
                MethodInfo deserialize = type.GetMethod("Deserialize", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                if (serialize != null && deserialize != null)
                {
                    return deserialize.Invoke(null, new object[] { (string)value });
                }
                else
                {
                    return JsonConvert.DeserializeObject(value, type, JsonSerializerSettings);
                }
            }
        }
        private void WriteNow(string path, T @object)
        {
            if (FilePath == null)
            {
                return;
            }
            if ((ReadAndWriteMode & ReadWriteModes.WRITE) == 0)
            {
                return;
            }

            OnWriteBegin?.Invoke(this, EventArgs.Empty);

            foreach (string propertyPath in MemberByPathAndName.Keys)
            {
                List<string> lines = new List<string> { _FilePrepend };

                // Get all properties
                List<MemberInfo> memberInfos = MembersByPath[propertyPath];

                // Write properties
                string sectionPrevious = "";
                for (int i = 0; i < memberInfos.Count; i++)
                {
                    MemberInfo memberInfo = memberInfos[i];
                    MapperAttributeBundle attributeBundle = AttributeBundles[memberInfo];
                    string section = attributeBundle.Section;

                    if (!String.Equals(section, sectionPrevious, StringComparison.Ordinal))
                    {
                        sectionPrevious = section;
                        lines.Add("");
                        lines.Add(Enwidth($"# {section} #", "", "#"));
                        lines.Add("");
                    }
                    if (attributeBundle.Comment != null)
                    {
                        string[] commentLines = attributeBundle.Comment.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string commentLine in commentLines)
                        {
                            lines.Add(CommentPrefix + commentLine);
                        }
                    }

                    Type type = GetType(memberInfo);
                    object value = GetValue(@object, memberInfo);

                    string valueString;
                    if (TypeToCustom.TryGetValue(type, out var custom))
                    {
                        valueString = custom.Serialize(@object, memberInfo);
                    }
                    else
                    {
                        if (TypeToConverter.TryGetValue(type, out var converter))
                        {
                            type = converter.TypeFinal;
                            value = converter.Serialize(value);
                        }

                        valueString = Serialize(type, value, attributeBundle.UseQuotes, attributeBundle.Password);
                    }

                    lines.Add(Enwidth(memberInfo.Name, valueString));
                }

                // Bottom line
                lines.Add("");
                lines.Add(Enwidth("#", "", "#"));
                lines.Add("");

                lines.Add(_FileAppend);

                // Now write to disk

                string pathAbsolute = Path.GetFullPath(path);
                string directory = Path.GetDirectoryName(pathAbsolute);
                bool directoryExists = Directory.Exists(directory);
                bool fileExists = File.Exists(pathAbsolute);

                // Create directory if none
                if (!directoryExists)
                {
                    DirectoryCreate(directory);
                }

                if (_RecoveryMode == RecoveryModes.NONE)
                {
                    File.WriteAllLines(pathAbsolute, lines);
                }
                else if (_RecoveryMode == RecoveryModes.BACKUP)
                {
                    string pathOriginal = pathAbsolute;
                    string pathBackup = $"{pathAbsolute}.{_RecoveryModeBackupExtension}";
                    // Create backup
                    if (fileExists)
                    {
                        File.Delete(pathBackup);
                        File.Copy(pathOriginal, pathBackup);
                    }
                    // Write to the original
                    File.WriteAllLines(pathOriginal, lines);
                    // Clear backup
                    if (fileExists)
                    {
                        File.Delete(pathBackup);
                    }
                }
                else if (_RecoveryMode == RecoveryModes.COPY)
                {
                    string pathOriginal = pathAbsolute;
                    string pathCopy = $"{pathAbsolute}.{_RecoveryModeCopyExtension}";
                    // Delete copy
                    File.Delete(pathCopy);
                    //File.Copy(pathOriginal, pathCopy);
                    // Write to the copy
                    File.WriteAllLines(pathCopy, lines);
                    // Substitute original with the copy
                    File.Delete(pathOriginal);
                    File.Move(pathCopy, pathOriginal);
                }
                else
                {
                    throw new NotImplementedException($"Mode {_RecoveryMode} is not supported.");
                }
            }

            OnWriteEnd?.Invoke(this, null);
        }
        #endregion

        #region Heplpers
        private string FormatString(string @string, bool useQuotes)
        {
            //// Append doublequotes
            //foreach (char character in new char[] { ' ', '\r', '\n' })
            //if (param_string.Contains(character))
            //{
            //    return $"\"{param_string.Replace("\"", "\"\"")}\"";
            //}
            if (useQuotes)
            {
                throw new NotImplementedException();
            }
            return @string;
        }
        private string ParseString(string @string, bool useQuotes)
        {
            //// Strip doublequotes
            //if (param_string.Length >= 2 && param_string.First() == '"' && param_string.Last() == '"')
            //{
            //    return param_string.Substring(1, param_string.Length - 2).Replace("\"\"", "\"");
            //}
            if (useQuotes)
            {
                throw new NotImplementedException();
            }
            return @string;
        }
        private string FormatDateTime(DateTime dateTime)
        {
            return dateTime.ToString(_DateTimeFormat);
        }
        private DateTime ParseDateTime(string @string, CultureInfo cultureInfo)
        {
            if (DateTime.TryParseExact(@string, _DateTimeFormat, cultureInfo, DateTimeStyles.AdjustToUniversal, out DateTime dateTime))
            {
                return dateTime;
            }
            return DateTime.MinValue;
        }
        private string FormatTimeSpan(TimeSpan timeSpan)
        {
            return timeSpan.ToString(_TimeSpanFormat);
        }
        private TimeSpan ParseTimeSpan(string @string, CultureInfo cultureInfo)
        {
            if (TimeSpan.TryParseExact(@string, _TimeSpanFormat, cultureInfo, out TimeSpan timeSpan))
            {
                return timeSpan;
            }
            return TimeSpan.MinValue;
        }
        private string Enwidth(string name, string value = "", string symbol = " ")
        {
            StringBuilder stringBuilder = new StringBuilder();

            // Insert property name
            stringBuilder.Append(name);
            // At least one delimiter symbol has to be presented
            stringBuilder.Append(symbol);
            // Insert delimiter symbols until filled up to necessary width
            int paddingLength = _TextWidth - name.Length - (value ?? "").Length;
            for (int i = 1; i < paddingLength; i++)
            {
                stringBuilder.Append(symbol);
            }
            // Insert property value
            stringBuilder.Append(value ?? "");

            return stringBuilder.ToString();
        }
        private string[] StringToArray(string @string, bool quoted = true)
        {
            if (quoted)
            {
                List<string> strings = new List<string>();
                //bool insideValue = false;
                for (int i = 0; i < @string.Length; i++)
                {
                    // Look for an opening doublequote
                    for (; i < @string.Length; i++)
                    {
                        if (@string[i] == '"')
                        {
                            break;
                        }
                    }

                    // First doublequote found
                    int start = ++i;

                    // Look for an closing doublequote
                    for (; i < @string.Length; i++)
                    {
                        if (@string[i] == '"')
                        {
                            // Check for double doublequote
                            if (i + 1 < @string.Length && @string[i + 1] == '"')
                            {
                                i++;
                                continue;
                            }
                            // Single doublequote found
                            break;
                        }
                    }

                    // Closing doublequote found
                    int end = i;

                    if (start == end)
                    {
                        strings.Add("");
                    }
                    else if (start > @string.Length - 2 || end > @string.Length - 1)
                    {
                        // Just skip
                        ;
                    }
                    else
                    {
                        // Add to the array
                        strings.Add(@string.Substring(start, end - start).Replace("\"\"", "\""));
                    }
                }
                return strings.ToArray();
            }
            else
            {
                return @string.Split(new [] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(lambda_string => lambda_string.Trim(new char[] { ' ', '\t'  })).ToArray();
            }
        }
        private string ArrayToString(IEnumerable<string> array, bool quoted = true)
        {
            return ArrayToString(array.ToArray<string>(), quoted);
        }
        private string ArrayToString(string[] array, bool quoted = true)
        {
            StringBuilder stringBuilder = new StringBuilder();

            string @string = "";

            if (quoted)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    if (i != 0)
                    {
                        stringBuilder.Append(", ");
                    }

                    stringBuilder.Append("\"");
                    stringBuilder.Append(array[i].Replace("\"", "\"\""));
                    stringBuilder.Append("\"");
                }
                @string = stringBuilder.ToString();
            }
            else
            {
                for (int i = 0; i < array.Length; i++)
                {
                    if (i != 0)
                    {
                        stringBuilder.Append(" ");
                    }

                    stringBuilder.Append(array[i]);
                }
                @string = stringBuilder.ToString();
            }

            stringBuilder.Clear();

            return @string;
        }
        private static MethodInfo GetMethod<T1>(Expression<Action<T1>> lambda)
        {
            MethodCallExpression call = lambda.Body as MethodCallExpression;
            if (call == null)
            {
                throw new ArgumentException($"Expression body was expected to be a method call, but was '{lambda.Body}' ({call.GetType()})");
            }
            return call.Method;
        }
        private static void DirectoryCreate(string directory)
        {
            // Full path
            string pathFull = Path.GetFullPath(directory);
            // Individual path parts
            string[] pathParts = pathFull.Split(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });

            StringBuilder stringBuilder = new StringBuilder();
            foreach (string pathPart in pathParts)
            {
                stringBuilder.Append(pathPart).Append(Path.DirectorySeparatorChar);
                string supDirectory = stringBuilder.ToString();
                if (!Directory.Exists(supDirectory))
                {
                    Directory.CreateDirectory(supDirectory);
                }
            }
        }
        public static Type GetType(MemberInfo memberInfo)
        {
            return memberInfo.MemberType == MemberTypes.Property ? ((PropertyInfo)memberInfo).PropertyType : ((FieldInfo)memberInfo).FieldType;
        }
        public static void SetValue(object @object, MemberInfo memberInfo, object value)
        {
            if (memberInfo.MemberType == MemberTypes.Property)
            {
                ((PropertyInfo)memberInfo).SetValue(@object, value);
            }
            else
            {
                ((FieldInfo)memberInfo).SetValue(@object, value);
            }
        }
        public static T GetValue<T>(object @object, MemberInfo memberInfo)
        {
            return (T)GetValue(@object, memberInfo);
        }
        public static object GetValue(object @object, MemberInfo memberInfo)
        {
            return memberInfo.MemberType == MemberTypes.Property ? ((PropertyInfo)memberInfo).GetValue(@object) : ((FieldInfo)memberInfo).GetValue(@object);
        }
        #endregion
    }
}
