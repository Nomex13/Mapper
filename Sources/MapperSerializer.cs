﻿using System;

namespace Iodynis.Libraries.Mapping
{
    internal class MapperSerializer
    {
        internal readonly Type Type;
        private readonly Func<object, string> Serializer;
        private readonly Func<string, object> Deserializer;

        internal MapperSerializer(Type type, Func<object, string> serializer, Func<string, object> deserializer)
        {
            Type = type;
            Serializer = serializer;
            Deserializer = deserializer;
        }

        internal string Serialize(object @object) => Serializer(@object);
        internal object Deserialize(string @string) => Deserializer(@string);
    }
}
