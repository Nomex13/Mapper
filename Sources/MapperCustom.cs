﻿using System;
using System.Reflection;

namespace Iodynis.Libraries.Mapping
{
    internal class MapperCustom
    {
        private readonly Func<Type, bool> Checker;
        private readonly Func<object, MemberInfo, string> Serializer;
        private readonly Action<object, MemberInfo, string> Deserializer;

        internal MapperCustom(Func<Type, bool> checker, Func<object, MemberInfo, string> serializer, Action<object, MemberInfo, string> deserializer)
        {
            Checker = checker;
            Serializer = serializer;
            Deserializer = deserializer;
        }

        internal bool Check(Type type) => Checker(type);
        internal string Serialize(object @object, MemberInfo memberInfo) => Serializer(@object, memberInfo);
        internal void Deserialize(object @object, MemberInfo memberInfo, string @string) => Deserializer(@object, memberInfo, @string);
    }
}
