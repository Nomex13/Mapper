﻿using System;

namespace Iodynis.Libraries.Mapping
{
    /// <summary>
    /// Custom file path for the mapped property value.
    /// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class MapperPath : Attribute
	{
        /// <summary>
        /// Path to the custom file.
        /// </summary>
		public string Path { get; }
        /// <summary>
        /// Custom file path for the mapped property value.
        /// </summary>
		public MapperPath(string path = null)
		{
			Path = path;
		}
	}
}
