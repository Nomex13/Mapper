﻿using System;

namespace Iodynis.Libraries.Mapping
{
	[AttributeUsage(AttributeTargets.Property)]
	public class MapperPosition : Attribute
	{
		public int Position { get; }
		public MapperPosition(int position = 0)
		{
			Position = position;
		}
	}
}
