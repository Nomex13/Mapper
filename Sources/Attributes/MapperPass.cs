﻿using System;

namespace Iodynis.Libraries.Mapping
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MapperPass : Attribute
    {
		public int Pass { get; }
		public MapperPass(int pass)
		{
			Pass = pass;
		}
    }
}
