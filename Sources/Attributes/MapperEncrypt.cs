﻿using System;

namespace Iodynis.Libraries.Mapping
{
	[AttributeUsage(AttributeTargets.Property)]
	public class MapperEncrypt : Attribute
	{
		private readonly string _Password = null;
		public string Password
		{
			get
			{
				return _Password;
			}
		}
		public MapperEncrypt()
		{
			_Password = null;
		}
		public MapperEncrypt(string password)
		{
			_Password = password;
		}
	}
}
