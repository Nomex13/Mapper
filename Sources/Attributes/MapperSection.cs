﻿using System;

namespace Iodynis.Libraries.Mapping
{
    /// <summary>
    /// Section in the file to store the mapped property value.
    /// </summary>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class MapperSection : Attribute
	{
        /// <summary>
        /// Name of the section in the file.
        /// </summary>
		public string Section { get; }
        /// <summary>
        /// Section in the file to store the mapped property value.
        /// </summary>
		public MapperSection(string section = null)
		{
			Section = section;
		}
	}
}