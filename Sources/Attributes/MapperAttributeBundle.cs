﻿using System;
using System.Reflection;

namespace Iodynis.Libraries.Mapping
{
    internal class MapperAttributeBundle
	{
		public MemberInfo MemberInfo { get; }
		public bool HasAtLeastOneAttribute { get; }

		public int Pass { get; }
		public string Path { get; }
		public string Section { get; }
		public int Position { get; }
		public string Name { get; }
		public bool UseQuotes { get; }
		public string Comment { get; }
		public string Password { get; }

		public MapperAttributeBundle(MemberInfo memberInfo, int defaultPass, string defaultPath, string defaultSection, int defaultPosition, string defaultName, bool defaultUseQuotes, string defaultPassword)
		{
            MemberInfo = memberInfo;

            MapperPass AttributePass = (MapperPass)Attribute.GetCustomAttribute(MemberInfo, typeof(MapperPass));
            MapperPath AttributePath = (MapperPath)Attribute.GetCustomAttribute(MemberInfo, typeof(MapperPath));
            MapperSection AttributeSection = (MapperSection)Attribute.GetCustomAttribute(MemberInfo, typeof(MapperSection));
            MapperPosition AttributePosition = (MapperPosition)Attribute.GetCustomAttribute(MemberInfo, typeof(MapperPosition));
            MapperName AttributeName = (MapperName)Attribute.GetCustomAttribute(MemberInfo, typeof(MapperName));
            MapperComment AttributeComment = (MapperComment)Attribute.GetCustomAttribute(MemberInfo, typeof(MapperComment));
            MapperQuotes AttributeUseQuotes = (MapperQuotes)Attribute.GetCustomAttribute(MemberInfo, typeof(MapperQuotes));
            MapperEncrypt AttributeEncryption = (MapperEncrypt)Attribute.GetCustomAttribute(MemberInfo, typeof(MapperEncrypt));

			Pass = AttributePass != null ? AttributePass.Pass : defaultPass;
			Path = AttributePath != null ? AttributePath.Path : defaultPath;
			Section = AttributeSection != null ? AttributeSection.Section : defaultSection;
			Position = AttributePosition != null ? AttributePosition.Position : defaultPosition;
			Name = AttributeName != null ? AttributeName.Name : defaultName;
			Comment = AttributeComment != null ? AttributeComment.Comment : null;
			UseQuotes = AttributeUseQuotes != null ? AttributeUseQuotes.UseQuotes : defaultUseQuotes;
			Password = AttributeEncryption != null ? (AttributeEncryption.Password ?? defaultPassword) : null; // Encrypt only if the attribute is explicitly specified

			HasAtLeastOneAttribute =
				AttributePass != null ||
				AttributePath != null ||
				AttributeSection != null ||
				AttributePosition != null ||
				AttributeName != null ||
				AttributeComment != null ||
				AttributeUseQuotes != null ||
				AttributeEncryption != null;
		}
	}
}
