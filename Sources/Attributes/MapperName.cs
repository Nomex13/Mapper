﻿using System;

namespace Iodynis.Libraries.Mapping
{
    /// <summary>
    /// Custom name for the prpoerty.
    /// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class MapperName : Attribute
	{
        /// <summary>
        /// Name to use instead of the properties own one.
        /// </summary>
		public string Name { get; }
        /// <summary>
        /// Custom name for the prpoerty.
        /// </summary>
		public MapperName(string name = null)
		{
			Name = name;
		}
	}
}
