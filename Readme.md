# Mapper

A serialization/deserialization library. Stores properties marked with `[Mapper...]` attributes that have a `...Changed` events in a human-readable pretty-formatted files.

Uses https://github.com/JamesNK/Newtonsoft.Json for complex types.
Supports writing to an intermediate file and swapping the original one, including check and recovery at initialization for such a file left from previously interrupted operation.
Supports some customization to the file, like line width, prefix, custom boolean values, custom serialization/deserialization methods for specific types.

Writes to the file in a separate thread after a changable delay, so multiple sequential changes are written in one operation.

## Dependencies

Mapper is dependent on the https://gitlab.com/iodynis/Encryptor project.

## Usage

Add attributes to the properties you want to serialize/deserialize:

```csharp
[MapperName("AnotherNameForVolumeMusic")]
[MapperSection("Simple")]
```

Create and initialize the mapper:

```csharp
var mapper = new Mapper<TestObject>(testObject, "test.cfg");
mapper.Initialize();
```

Values will be read from the file and applied to the object immediately. Any further changes made to the object will be reflected in the file.

## Example

Serialized files look like this:

```
# Audio ########################################################################

VolumeMaster                                                                 1.0
AnotherNameForVolumeMusic                                                   0.69
VolumeSound                                                                 0.77

# System #######################################################################

Language                                                                      ru
ShowHints                                                                     on
ShowTutorial                                                                  on

# Video ########################################################################

WindowSize                                                             "400, 500"
FieldOfView                                                                   90
ResolutionX                                                                    0
ResolutionY                                                                    0

################################################################################

```

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.